#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This small script loops through a chunk of midi-files and makes some
# processing (if midi file is single track). Renders mp3 when processing is
# done.

# The midi processing is only done when the midi-file is single-track
# because, the processins is probably not what you want if you have
# a multitrack midifile anyway.
#
# Processing steps:
#
# 1. Adds a ProgramChangeEvent if not present (i.e. change the
#    instrument used. If the filename contains the word "orgel" use
#    the instrument Church Organ (19) otherwise use Grand Piano (0).
#
# 2. Trim the midi-file so no there is no no silence in the
#    beginning.
#
#
# Requirements:
#
# - python-midi (https://github.com/vishnubob/python-midi)
# - timidity
# - avconv (ffmpeg)


import glob
import os
import subprocess

import midi


TIMIDITY_CMD = 'timidity'
FFMPEG_CMD = 'avconv'


def process_midi(fname_in, fname_out):
    """
    Main function to process midi-file

    Reading and writting midi-files.
    """
    pattern = midi.read_midifile(fname_in)

    # Multitrack file check
    if len(pattern) > 1:
        print 'WARNING: "%s"" is a multitrack file and was not' \
              'processed.' % (fname_in,)
    else:
        # Insert a program change event for for midi if not present
        pattern = add_PCE(pattern, fname_in)

        # Remove initial silence
        pattern = trim_midi(pattern)

    midi.write_midifile(fname_out, pattern)


def add_PCE(pattern, fname_in, trackno=1):
    """
    Inserts ProgramChangeEvent on the first track of a midi-file if
    not present.

    Return python-midi pattern.

    """
    track = pattern[trackno-1]
    pattern_types = [type(i) for i in track]
    has_ProgramChangeEvent = midi.ProgramChangeEvent in pattern_types
    if has_ProgramChangeEvent:
        # Don't add a ProgramChangeEvent if already present
        pass
    else:
        # Get instrument to use
        if 'orgel' in fname_in.lower():
            instrument = 19  # Church organ
        else:
            instrument = 0  # Default = Grand Piano

        # Add a ProgramChangeEvent just before first NoteOnEvent
        idx = pattern_types.index(midi.NoteOnEvent)
        channel = track[idx].channel
        track.insert(idx, midi.ProgramChangeEvent(tick=0, channel=channel,
                                                  data=[instrument]))
    return pattern


def trim_midi(pattern, trackno=1, initial_tick=10):
    """
    Remove initial silence of midi-file

    Returns python-midi pattern.
    """
    track = pattern[trackno-1]

    # Find first NoteOnEvent and set fixed tick-value
    pattern_types = [type(i) for i in track]
    idx = pattern_types.index(midi.NoteOnEvent)
    track[idx].tick = initial_tick

    return pattern


def render_mp3(inputfile):
    # Build output-filename
    root, _ = os.path.splitext(inputfile)
    outputf = root + '.mp3'
    render_cmd = [TIMIDITY_CMD, inputfile, '--output-24bit',
                  '-A120', '-Ow', '-o', '-']
    mp3_cmd = [FFMPEG_CMD, '-i', '-', '-acodec', 'libmp3lame',
               '-ab', '256k', outputf]
    ps = subprocess.Popen(render_cmd, stdout=subprocess.PIPE)
    subprocess.check_output(mp3_cmd, stdin=ps.stdout)
    ps.wait()


if __name__ == '__main__':
    indir = os.getcwd()
    outdir = os.path.join(indir, 'processed')

    # Make output directory if not present
    try:
        os.makedirs(outdir)
        print 'Created: ' + outdir
    except OSError:
        pass

    # Loop throug midi-files
    for fname in glob.iglob(os.path.join(indir, '*.mid')):
        _, tail = os.path.split(fname)
        outputf = os.path.join(outdir, tail)

        process_midi(fname, outputf)
        render_mp3(outputf)
