Midi-fix
========

This small script loops through a chunk of midi-files and makes some
processing (if midi file is single track). Renders mp3 when processing is
done.

The midi processing is only done when the midi-file is single-track
because, the processins is probably not what you want if you have
a multitrack midifile anyway.

Processing steps
----------------

1. Adds a ProgramChangeEvent if not present (i.e. change the
   instrument used. If the filename contains the word "orgel" use
   the instrument Church Organ (19) otherwise use Grand Piano (0).

2. Trim the midi-file so no there is no no silence in the
   beginning.

Requirements
------------

  * python-midi (https://github.com/vishnubob/python-midi)
  * timidity
  * avconv (ffmpeg)